package watcher

import (
	"bytes"
	"io"
	"log"
	"net/http"
)

const URL = "https://cinepolis.com.pa/Sinopsis.aspx/GetNowPlayingByMovie"



func GetMoviesTimes() (string, error) {
	var jsonStr = []byte(`{"codigoPelicula":"spider-man-sin-camino-a-casa","codigoCiudad":"panama-panama"}`)
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err)
	}

	return string(b), err
}