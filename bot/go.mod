module example.com/bot

go 1.17

require (
	example.com/watcher v0.0.0-00010101000000-000000000000
	github.com/bwmarrin/discordgo v0.23.2
)

require (
	github.com/gorilla/websocket v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20181030102418-4d3f4d9ffa16 // indirect
)

replace example.com/watcher => ../watcher
