package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"example.com/watcher"
	"github.com/bwmarrin/discordgo"

)

// Variables used for command line parameters
var (
	Token string
)

func init() {

	flag.StringVar(&Token, "t", "MTY4MTc1Nzg0NTQ3MDU3NjY0.Vwhf5w.51CznNgkcfg-HVbYEid8f7ufuJQ", "Bot Token")
	flag.Parse()
}

func main() {

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	
	if m.Author.ID == s.State.User.ID {
		return
	}
	// If the message is "ping" reply with "Pong!"
	if m.Content == "Activar" {
		s.ChannelMessageSend(m.ChannelID, "Hola, Avisare cuando salga *Spider-Man: No Way Home*. Si me rompo, avisenle a @Tiz")
		sum := 1
		for sum < 1900800 {
			time.Sleep(5 * time.Second)
			message, err := watcher.GetMoviesTimes()
			if err != nil {
				fmt.Println("error opening connection,", err)
			}
			
			if "{\"d\":null}" != message{
				s.ChannelMessageSend(m.ChannelID, "@everyone YA SALIO LA PREVENTAAAAA!! VAYAN A COMPRAR YAAAA!")
			}
			
			fmt.Println(message)
			
		}
	}
	if m.Content == "debugmode" {
		s.ChannelMessageSend(m.ChannelID, "Hola, Avisare cuando salga *Spider-Man: No Way Home*. Si me rompo, avisenle a @Tiz")
		sum := 1
		for sum < 1900800 {
			time.Sleep(5 * time.Second)
			message, err := watcher.GetMoviesTimes()
			if err != nil {
				fmt.Println("error opening connection,", err)
			}
			if "{\"d\":null}" != message{
				s.ChannelMessageSend(m.ChannelID, "@everyone YA SALIO LA PREVENTAAAAA!! VAYAN A COMPRAR YAAAA!")
			}
			
			fmt.Println(message)
			
		}
	}
/*
	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}
	if m.Content == "cute" {
		message, err := watcher.GetMoviesTimes()
		if err != nil {
			fmt.Println("error opening connection,", err)
		}
		fmt.Println(message)
		s.ChannelMessageSend(m.ChannelID, "@everyone "+message)
	}
	*/
}